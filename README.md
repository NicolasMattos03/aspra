![DWAD](https://github.com/JDGA1997/ASPRA-Movil/assets/105946879/4bd8b57c-8cb3-4d67-92a8-bc83ac8733f4)
<br></br>

<div align="center">

# 🐾 ASPRA WEB 🐾
![imagen](https://raw.githubusercontent.com/AS-PR-A/ASPRA-Web/main/Frontend/animalesCommerce/src/assets/img/ASPRA.png)
<br>

<br />
 
## Funcionalidades del sitio web 📑📑

Registro

Inicio de Sesion

Reportar animal perdido/maltratado

Contacto con veterinarios

Donaciones a traves de Mercado Pago

Boton flotante para descargar ASPRA Movil

<br>

## Integrantes  👩‍💻👨🏼‍💻

| Nombre          | Apellido            | GitHub                                                | Comision |    Rol Scrum    |
|-----------------|---------------------|-------------------------------------------------------|----------|-----------------|
| Juan Diego      | González Antoniazzi | [JDGA1997](https://github.com/JDGA1997)               |   2      |  Scrum Master   |
| Axel Ezequiel   | Montivero           | [Ezmant](https://github.com/Ezmant)                   |   2      |   Developer     |
| Fabiana Jazmin  | Amato               | [AmatoJazmin](https://github.com/AmatoJazmin)         |   1      |   Developer     |
| Nicolás         | Mattos              | [NicolasMattos03](https://github.com/NicolasMattos03) |   2      |   Developer     |
| Melanie         | Reyes               | [MelanieR24](https://github.com/MelanieR24)           |   3      |      Developer  |
| Alejandro       |     Moreno          | [MoCAlejandro](https://github.com/MoCAlejandro)       |   2      |   Developer     |
  




<br>

### Front-End ⚛️

HTML5

CSS3

Javascript

Bootstrap

Angular

### Back-End 💻
Python para arquitectura MVC y POO.

Django

Typescript

### Base de Datos 📚
MySQL

<br>

## Más de ASPRA

### [Documento IEEE830](https://docs.google.com/document/d/1yoGYpTMU1NPqZDSuM2iEQeQGeHVx1eCX/edit?usp=sharing&ouid=103416615054896105402&rtpof=true&sd=true)

### [Diagrama de Caso de Uso](https://github.com/AS-PR-A/ASPRA-Web/blob/main/Documentacion/ASPRA%20web%20Diagrama%20Caso%20de%20Uso%20(Actualizado).pdf)

### [Diagrama de Entidad-Relacion (DER)](https://github.com/AS-PR-A/ASPRA-Web/blob/main/Documentacion/DER%20ASPRA%20web%20(Actualizado).pdf)

### [Diagrama de Clases](https://github.com/AS-PR-A/ASPRA-Web/blob/main/Documentacion/ASPRA%20web%20diagrama%20de%20clases(actualizado).pdf)

### [Modelo Relacional](https://github.com/AS-PR-A/ASPRA-Web/blob/main/Documentacion/Modelo%20relacional.pdf)

### [Script de BBDD](https://github.com/AS-PR-A/ASPRA-Web/blob/main/Documentacion/script.sql)

### [Mapa de Sitio](https://github.com/AS-PR-A/ASPRA-Web/blob/main/Documentacion/Mapa%20de%20sitio%20ASPRA%20Web%20(Actualizado).pdf)

### [Wiki](https://github.com/AS-PR-A/ASPRA-Web/wiki)

### [Tablero Kanban](https://github.com/orgs/AS-PR-A/projects/8/views/1)

### [Milestone](https://github.com/AS-PR-A/ASPRA-Web/milestones)

### [Sprint 1](https://github.com/AS-PR-A/ASPRA-Web/milestone/1)

### [Issues](https://github.com/AS-PR-A/ASPRA-Web/issues?q=)

### [Testing](https://github.com/AS-PR-A/ASPRA-Web/tree/main/Testing)

### [ASPRA Movil](https://github.com/AS-PR-A/ASPRA-Movil)

</div>
